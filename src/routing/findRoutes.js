import _ from 'lodash';
import {
  calcMRTTime,
  calcWaitTime,
  calcWalkingTime,
  countDistance, findLineByID,
  findNearestStations,
  findStationByKey,
} from './utils';
import MRTGraph from './mrtGraph';

// Returns the best routes between the origin and destination.
export default function findRoutes(origin, destination) {
  const nearestOriginStations = findNearestStations(origin);
  const nearestDestStations = findNearestStations(destination);

  // get the nearest lines between the nearest stations of origin and destination
  let possibleRoutes = [];
  const mrtGraph = new MRTGraph();
  _.forEach(nearestOriginStations, (station1) => {
    _.forEach(nearestDestStations, (station2) => {
      possibleRoutes = _.concat(possibleRoutes,
        mrtGraph.giveTransferRoutes(station1.key, station2.key));
    });
  });

  // {method, route, minutes}
  let routeWithTimeArray = _.map(possibleRoutes, (route) => {
    if (route.length === 0) {
      // simply walk without mrt
      return {
        steps: [{
          type: 'walk',
          from: origin,
          to: destination,
          minutes: calcWalkingTime(countDistance(origin, destination)),
        }],
        minutes: calcWalkingTime(countDistance(origin, destination)),
      };
    }
    const routeArray = [];

    let minutes = 0;
    // needs mrt
    const startStation = findStationByKey(_.head(_.head(route).stations).key);
    const endStation = findStationByKey(_.last(_.last(route).stations).key);

    routeArray.push({
      type: 'walk',
      from: 'your start point',
      to: startStation.name,
      minutes: calcWalkingTime(countDistance(origin, startStation)),
    });
    minutes += calcWalkingTime(countDistance(origin, startStation));
    _.forEach(route, ({ stations, line }) => {
      // find the line of this part of route
      routeArray.push({
        type: 'mrt',
        line: findLineByID(line),
        from: _.head(stations).name,
        to: _.last(stations).name,
        stations: _.map(stations, t => t.key),
        minutes: calcMRTTime(stations),
      });
      minutes += calcMRTTime(stations);
      minutes += calcWaitTime(); // need to add wait time in mrt
    });
    routeArray.push({
      type: 'walk',
      from: endStation.name,
      to: 'your destination',
      minutes: calcWalkingTime(countDistance(endStation, destination)),
    });
    minutes += calcWalkingTime(countDistance(endStation, destination));
    return {
      steps: routeArray,
      minutes,
    };
  });

  // add a direct walk route
  const directWalkingMinutes = _.ceil(calcWalkingTime(countDistance(origin, destination)) * 1.7);
  // we can't walk in straight line, so I set the time to be multiplied by 1.7
  routeWithTimeArray.push({
    steps: [{
      type: 'walk',
      from: 'start point',
      to: 'destination point',
      minutes: directWalkingMinutes,
    }],
    minutes: directWalkingMinutes,
  });

  routeWithTimeArray = _.uniqBy(_.sortBy(routeWithTimeArray, t => t.minutes), JSON.stringify);
  const results = [];
  results.push(routeWithTimeArray.shift());
  while (routeWithTimeArray.length && results.length < 6) {
    const tempRoute = routeWithTimeArray.shift();
    const lastRoute = _.last(results);
    if (tempRoute.minutes < lastRoute.minutes + 15) {
      // 15 minutes is reasonable for possibly better routes
      results.push(tempRoute);
    } else {
      break;
    }
  }

  return results;
}
