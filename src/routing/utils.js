import _ from 'lodash';
import { stations, lines } from './mrt';

// ref: https://en.wikipedia.org/wiki/Preferred_walking_speed
const WALKING_SPEED_PER_SECOND = 1.4;
const MINUTES_PER_MRT = 3;
const MINUTES_TO_WAIT = 5;

// calculate distance between two coordinate points
// reference: https://www.geodatasource.com/developers/javascript
// https://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-longitude-points-haversine-formula
export const countDistance = (point1, point2) => {
  const lat1 = point1.lat; const
    lng1 = point1.lng;
  const lat2 = point2.lat; const
    lng2 = point2.lng;
  const deg2rad = deg => deg * (Math.PI / 180);
  const R = 6371; // Radius of the earth in km
  const dLat = deg2rad(lat2 - lat1); // deg2rad below
  const dLon = deg2rad(lng2 - lng1);
  const a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
    + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2))
    * Math.sin(dLon / 2) * Math.sin(dLon / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  return R * c * 1000;
};

export const findNearestStations = ({ lat, lng }) => {
  // sometimes there may be several stations near uesr's location,
  // and different choice would result in different routes
  if (!lat || !lng) {
    // console.error('Invalid location found in findNearestStation method!');
    return '';
  }
  const stationsKeyWithDistance = _.map(stations, (value, key) => ({
    key,
    distance: countDistance(value, { lat, lng }),
  }));
  const sortedStations = _.sortBy(stationsKeyWithDistance,
    station => station.distance);
  const nearestStations = [];
  nearestStations.push(sortedStations.shift());
  while (sortedStations.length) {
    const nextPossibleStation = sortedStations.shift();
    const lastStation = _.last(nearestStations);
    if (nextPossibleStation.distance < lastStation.distance + 400) {
      // also a nearby station, add it into possible lists
      nearestStations.push(nextPossibleStation);
    } else {
      break;
    }
  }

  // sort stations with their distance to the given point
  return _.map(nearestStations, (station) => {
    const ret = _.clone(station);
    ret.walk_distance = station.distance;
    return ret;
  });
};

// how many minutes need to walk there?
export const calcWalkingTime = meter => _.ceil(meter / (WALKING_SPEED_PER_SECOND * 60));

// how many minutes need for taking mrt line?
export const calcMRTTime = stationsArr => _.size(stationsArr) * MINUTES_PER_MRT;
// wait for a train to arrive?
export const calcWaitTime = () => MINUTES_TO_WAIT;

export const findStationByKey = key => stations[key];

export const findLineByID = id => lines[id];
