import _ from 'lodash';
import { stations, lines } from './mrt';

class Station {
  constructor(name, key) {
    this.name = name;
    this.key = key;
    this.neighbors = {}; // neighborStation -> routeName,
    // as one neighbor has only one route between them
  }

  addNeighbor(key, line) {
    this.neighbors[key] = line;
  }
}

export default class MRTGraph {
  constructor() {
    this.stationMap = new Map();

    _.forEach(stations, ({ name }, key) => {
      const station = new Station(name, key);
      this.stationMap.set(key, station);
    });

    _.forEach(lines, ({ route }, line) => {
      if (route) {
        for (let i = 0; i < route.length - 1; i += 1) {
          let station1 = this.stationMap.get(route[i]);
          let station2 = this.stationMap.get(route[i + 1]);
          if (station1 && station2) {
            station1.addNeighbor(station2.key, line);
          }
          // reverse
          station1 = this.stationMap.get(route[route.length - i - 1]);
          station2 = this.stationMap.get(route[route.length - i - 2]);
          if (station1 && station2) {
            station1.addNeighbor(station2.key, line);
          }
        }
      }
    });
  }

  findPath(key1, key2) {
    const paths = [];

    const bfs = () => {
      const queue = [key1];
      const visited = new Set();
      visited.add(key1);
      const predecessor = {};

      while (queue.length > 0) {
        const nodekey = queue.shift();

        const node = this.stationMap.get(nodekey);
        const neighbors = _.keys(node.neighbors);

        _.forEach(neighbors, (t) => {
          if (visited.has(t)) {
            return;
          }
          visited.add(t);
          if (t === key2) {
            const path = [t];
            let temp = nodekey;
            while (temp !== key1) {
              path.push(temp);
              temp = predecessor[temp];
            }
            path.push(temp);
            path.reverse();
            paths.push(path);
            return;
          }
          predecessor[t] = nodekey;
          queue.push(t);
        });
      }
    };

    const visited = new Set();
    // dfs with a threshold, no need to search for all graph
    const dfs = (nodekey, path, threshold = 100) => {
      if (visited.has(nodekey)) {
        return;
      }
      if (path.length > threshold) {
        return;
      }
      visited.add(nodekey);
      const node = this.stationMap.get(nodekey);
      const neighbors = _.keys(node.neighbors);
      _.forEach(neighbors, (t) => {
        if (visited.has(t)) {
          return;
        }
        const newPath = _.concat(path, [t]);
        if (t === key2) {
          paths.push(newPath);
          return;
        }
        dfs(t, newPath, threshold);
      });
    };

    bfs();
    if (paths.length > 0) {
      dfs(key1, [key1], _.head(paths).length * 2);
    } else {
      dfs(key1, [key1]);
    }
    return paths;
  }

  // return an array combined of arrays, in which an array is made up of stations in a same mrt line
  giveTransferRoutes(stationKey1, stationKey2) {
    if (_.isEqual(stationKey1, stationKey2)) {
      return [];
    }
    // use bfs algorithm to find the shortest path between two stations
    const routes = this.findPath(stationKey1, stationKey2);
    const readableRoutes = [];

    _.forEach(routes, (route) => {
      const readableRoute = [];
      let tempArray = [];
      let line = '';
      for (let i = 0; i < route.length; i += 1) {
        const lastStation = this.stationMap.get(route[i]);
        const thisStation = this.stationMap.get(route[i + 1]);

        if (_.isUndefined(thisStation)) {
          // at end of route
          readableRoute.push({
            line,
            stations: _.clone(tempArray),
          });
          break;
        }

        const betweenLine = lastStation.neighbors[thisStation.key];

        if (!_.isEqual(betweenLine, line)) {
          if (tempArray.length > 0) {
            readableRoute.push({
              line,
              stations: _.clone(tempArray),
            });
            tempArray = [];
          }
          line = betweenLine;
          tempArray.push(lastStation); // starter point
        }
        tempArray.line = betweenLine;
        tempArray.push(thisStation);
      }
      readableRoutes.push(readableRoute);
    });
    return readableRoutes;
  }
}
