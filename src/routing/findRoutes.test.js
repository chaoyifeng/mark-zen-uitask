import _ from 'lodash';
import findRoutes from './findRoutes';

describe('should be able to find nearest paths between two stations', () => {
  it('test case 1', () => {
    const results = findRoutes(
      {
        lat: 1.327257,
        lng: 103.946579,
      }, {
        lat: 1.357371,
        lng: 103.988836,
      },
    );
    expect(results.length).toBeGreaterThan(0);
    const { steps } = results[0];
    expect(steps).not.toBeNull();
    expect(steps.length).toBe(3); // walk + mrt + walk
    expect(_.isEqual(_.head(steps).from, {
      lat: 1.327257,
      lng: 103.946579,
    }));
    expect(_.isEqual(_.last(steps).from, {
      lat: 1.357371,
      lng: 103.988836,
    }));
  });


  it('test case 2', () => {
    const results = findRoutes(
      {
        lat: 1.281321,
        lng: 103.83897,
      }, {
        lat: 1.312078,
        lng: 103.796208,
      },
    );
    expect(results.length).toBeGreaterThan(0);
    const { steps } = results[0];
    expect(steps).not.toBeNull();
    expect(steps.length).toBe(4); // walk + mrt + mrt + walk
    expect(_.last(steps[1])).toBe(_.head(steps[2]));
    expect(_.isEqual(_.head(steps).from, {
      lat: 1.33737,
      lng: 103.69709,
    }));
    expect(_.isEqual(_.last(steps).from, {
      lat: 1.298593,
      lng: 103.845909,
    }));
  });
});
