import MRTGraph from './mrtGraph';

describe('MRTGraph can work properly', () => {
  let graph;

  beforeEach(() => {
    graph = new MRTGraph();
  });

  it('can be initialized', () => {
    expect(graph).not.toBeNull();
  });

  it('can give empty results with same stations given', () => {
    const routes = graph.giveTransferRoutes('chinatown', 'chinatown');
    expect(routes.length).toBe(0);
  });

  it('can give path based on groups of lines', () => {
    const routes = graph.giveTransferRoutes('chinatown', 'queenstown');
    expect(routes.length).toBeGreaterThan(1);
    expect(routes[0].length).toBe(2);
    expect(routes[0][0].stations.length).toBe(2); // NE
    expect(routes[0][1].stations.length).toBe(4); // EW
  });
});
