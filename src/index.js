import React from 'react';
import ReactDOM from 'react-dom';
import 'bulma/css/bulma.css';

import App from './components/App';
import './index.less';

ReactDOM.render(
  <App />,
  document.getElementById('root'),
);
