import React from 'react';

const WalkMan = () => (
  // just the walking man, haha
  <span className="icon">
    <i className="fa fa-walking" />
  </span>
);

export default WalkMan;
