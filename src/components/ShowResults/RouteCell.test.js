import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-15.4';
import TestRenderer from 'react-test-renderer';
import RouteCell from './RouteCell';

Enzyme.configure({ adapter: new Adapter() });

describe('RouteCell', () => {
  const route = {
    steps: [{
      type: 'walk', from: 'your start point', to: 'Chinatown', minutes: 0,
    }, {
      type: 'mrt',
      line: {
        name: 'North East Line',
        color: '#81a',
        route: ['harbourfront', 'outram_park', 'chinatown', 'clarke_quay', 'dhoby_ghaut', 'little_india', 'farrer_park', 'boon_keng', 'potong_pasir', 'woodleigh', 'serangoon', 'kovan', 'hougang', 'buangkok', 'sengkang', 'punggol'],
      },
      from: 'Chinatown',
      to: 'Dhoby Ghaut',
      stations: ['chinatown', 'clarke_quay', 'dhoby_ghaut'],
      minutes: 9,
    }, {
      type: 'mrt',
      line: {
        name: 'North South Line',
        color: '#d22',
        route: ['jurong_east', 'bukit_batok', 'bukit_gombak', 'choa_chu_kang', 'yew_tee', 'kranji', 'marsiling', 'woodlands', 'admiralty', 'sembawang', 'yishun', 'khatib', 'yio_chu_kang', 'ang_mo_kio', 'bishan', 'braddell', 'toa_payoh', 'novena', 'newton', 'orchard', 'somerset', 'dhoby_ghaut', 'city_hall', 'raffles_place', 'marina_bay', 'marina_south_pier'],
      },
      from: 'Dhoby Ghaut',
      to: 'City Hall',
      stations: ['dhoby_ghaut', 'city_hall'],
      minutes: 6,
    }, {
      type: 'mrt',
      line: {
        name: 'East West Line',
        color: '#093',
        route: ['pasir_ris', 'tampines', 'simei', 'tanah_merah', 'bedok', 'kembangan', 'eunos', 'paya_lebar', 'aljunied', 'kallang', 'lavender', 'bugis', 'city_hall', 'raffles_place', 'tanjong_pagar', 'outram_park', 'tiong_bahru', 'redhill', 'queenstown', 'commonwealth', 'buona_vista', 'dover', 'clementi', 'jurong_east', 'chinese_garden', 'lakeside', 'boon_lay', 'pioneer', 'joo_koon'],
      },
      from: 'City Hall',
      to: 'Bugis',
      stations: ['city_hall', 'bugis'],
      minutes: 6,
    }, {
      type: 'walk', from: 'Bugis', to: 'your destination', minutes: 0,
    }],
    minutes: 36,
  };

  it('should match snapshot with given props', () => {
    const tree = TestRenderer
      .create(
        <RouteCell route={route} />,
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should expand when user click', () => {
    const wrapper = shallow(<RouteCell route={route} />);
    expect(wrapper.state('expand')).toBe(false);
    wrapper.find('.route-cell-container-box').simulate('click');
    expect(wrapper.state('expand')).toBe(true);
  });
});
