import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';

import RouteCell from './RouteCell';

import './ShowResults.css';

const ShowResults = ({ results }) => {
  if (!results || results.length === 0) {
    return <div id="results-box" />;
  }
  return (
    <div id="results-box">
      <h5 className="title is-6">Suggested Routes for you:</h5>
      {_.map(results, (route, index) => <RouteCell route={route} key={index} />)}
    </div>
  );
};

ShowResults.propTypes = {
  results: PropTypes.instanceOf(Array),
};
ShowResults.defaultProps = {
  results: [],
};

export default ShowResults;
