import React from 'react';
import PropTypes from 'prop-types';
import './MRTSymbol.css';

const MRTSymbols = (props) => {
  const { line } = props;
  const { color, name } = line;
  return (
    <span style={{ backgroundColor: color }} className="mrt-symbol">{name}</span>
  );
};

MRTSymbols.propTypes = {
  line: PropTypes.instanceOf(Object).isRequired,
};

export default MRTSymbols;
