import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import MRTSymbols from './MRTSymbol';
import './RouteCell.css';
import WalkMan from './WalkMan';

class RouteCell extends Component {
  state = {
    expand: false,
  };

  constructor(props) {
    super(props);
    this.handleClickCell = this.handleClickCell.bind(this);
  }

  componentWillReceiveProps() {
    this.setState({
      expand: false,
    });
  }

  handleClickCell() {
    const { expand } = this.state;
    this.setState({
      expand: !expand,
    });
  }

  render() {
    const { route } = this.props;
    const { expand } = this.state;

    const renderShort = () => {
      const result = [];
      let { steps } = route;
      const firstStep = _.head(steps);
      if (firstStep && firstStep.type === 'walk' && firstStep.minutes > 0) {
        steps = _.drop(steps);
        const firstStepWalkString = `Start by ${firstStep.minutes} minutes walk.`;
        result.push(
          <span key={`walk${0}`}>
            <WalkMan />
            {firstStepWalkString}
          </span>,
        );
      }

      _.each(steps, (step, index) => {
        if (step.type === 'mrt') {
          if (_.size(result) > 0) {
            // add a separator
            result.push(<span key={`arrow${index}`} className="icon"><i className="fas fa-arrow-right" /></span>);
          }
          result.push(
            <MRTSymbols line={step.line} key={`mrtsymbol${index}`} />,
          );
        }
      });

      const lastStep = _.last(steps);
      if (lastStep && lastStep !== firstStep && lastStep.type === 'walk' && lastStep.minutes > 0) {
        if (_.size(result) > 0) {
          // add a separator
          result.push(<span key="arrowlast" className="icon"><i className="fas fa-arrow-right" /></span>);
        }
        const lastStepWalkString = `End by ${lastStep.minutes} minutes walk.`;
        result.push(
          <span key={`walk${2}`}>
            <WalkMan />
            {lastStepWalkString}
          </span>,
        );
      }
      return result;
    };

    const renderTotalMinutes = () => {
      const totalMinutesString = `${route.minutes} minutes`;
      return <p>{totalMinutesString}</p>;
    };

    const renderWalkMinutes = () => {
      let walkMinutes = 0;
      _.forEach(route.steps, (step) => {
        if (step.type === 'walk') {
          walkMinutes += step.minutes;
        }
      });
      const totalWalkMinutesString = `(Contains ${walkMinutes} minutes walk)`;
      return <p>{totalWalkMinutesString}</p>;
    };

    const renderLinesLong = () => {
      const result = [];
      if (route.steps.length === 1) {
        const step = route.steps[0];
        const walkString = `Just walk or ride a bike! It takes about ${step.minutes} minutes.`;
        result.push(
          <p key="walk">
            <WalkMan />
            {walkString}
          </p>,
        );
      } else {
        result.push(
          <ul key="ul">
            {(() => _.map(route.steps, (step, index) => {
              if (step.type === 'walk') {
                const walkString = `Walk from [${step.from}] to [${step.to}], about ${step.minutes} minutes.`;
                return (
                  <li key={index}>
                    <WalkMan />
                    {walkString}
                  </li>
                );
              }
              if (step.type === 'mrt') {
                const mrtString = `Take [${step.line.name}] from [${step.from}] station to [${step.to}] station, we need to take ${step.stations.length} stations, about ${step.minutes} minutes.`;
                return (
                  <li style={{ color: step.line.color }} key={index}>
                    <MRTSymbols line={step.line} key={`symbol${index}`} />
                    {mrtString}
                  </li>
                );
              }
              return <p key={index}>{step}</p>;
            }))()}
          </ul>,
        );
      }

      const totalMinutesString = `In total, we need about ${route.minutes} minutes`;
      result.push(<p className="minute-long" key="total">{totalMinutesString}</p>);
      return result;
    };

    const renderShortRouteCellContent = () => (
      <div className="route-cell-box">
        <div className="route-short">
          <div className="lines-short">{renderShort()}</div>
        </div>
        <div className="minute-short has-text-right">
          {renderTotalMinutes()}
          {renderWalkMinutes()}
        </div>
      </div>
    );

    const renderDetailRouteCellContent = () => (
      // detailed route when user clicked this cell
      <div className="route-cell-box">
        <div className="route-long">{renderLinesLong()}</div>
      </div>
    );

    return (
      <div className="route-cell-container-box box content is-size-7" onClick={this.handleClickCell}>
        {expand ? renderDetailRouteCellContent() : renderShortRouteCellContent()}
      </div>
    );
  }
}

RouteCell.propTypes = {
  route: PropTypes.instanceOf(Object).isRequired,
};

export default RouteCell;
