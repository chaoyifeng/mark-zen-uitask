import React, { Component } from 'react';
import InputBox from './InputBox';
import ShowResults from './ShowResults';

import './App.css';
import findRoutes from '../routing/findRoutes';

// This is only a placeholder to demonstrate the Google Maps API.
// You should reorganize and improve it.

class App extends Component {
  state = {};

  constructor(props) {
    super(props);
    this.handleClickFindRoutes = this.handleClickFindRoutes.bind(this);
    this.handleError = this.handleError.bind(this);
    this.clearResults = this.clearResults.bind(this);
  }

  handleClickFindRoutes(origin, destination) {
    if (!origin || !destination) {
      return;
    }
    const routes = findRoutes(origin, destination);
    this.setState({
      routes,
    });
  }

  handleError(hasError, message = '') {
    this.setState({
      hasError,
      message,
    });
  }

  clearResults() {
    this.setState({
      routes: [],
    });
  }

  render() {
    const { routes, hasError, message } = this.state;
    return (
      <div id="app" className="card is-fullheight">
        <header className="card-header">
          MRT Routes Finder
        </header>
        <div className="card-content">
          <div className="card-image">
            <InputBox
              handleClickFindRoutes={this.handleClickFindRoutes}
              handleError={this.handleError}
              clearResults={this.clearResults}
            />
            <hr />
            {
              hasError
                ? <div className="has-text-danger">{message}</div>
                : <ShowResults results={routes} />
            }
          </div>
        </div>
      </div>
    );
  }
}

export default App;
