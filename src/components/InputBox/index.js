import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import './InputBox.css';

class InputBox extends Component {
  state = {
    origin: undefined,
    destination: undefined,
  };

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.handleClear = this.handleClear.bind(this);
    this.handleDestinationKeyPress = this.handleDestinationKeyPress.bind(this);
  }

  componentDidMount() {
    const { handleError } = this.props;
    setTimeout(() => {
      if (!window.google || !window.google.maps) {
        handleError(true, 'Cannot connect to Google maps service, please check your network and refresh!');
        return;
      }
      const { SearchBox } = window.google.maps.places;
      const originSearch = new SearchBox(document.getElementById('origin'));
      originSearch.addListener('places_changed', () => {
        const places = originSearch.getPlaces();
        const location = places[0].geometry.location.toJSON();
        this.setState({
          origin: location,
        });
      });
      const destinationSearch = new SearchBox(document.getElementById('destination'));
      destinationSearch.addListener('places_changed', () => {
        const places = destinationSearch.getPlaces();
        const location = places[0].geometry.location.toJSON();
        this.setState({
          destination: location,
        });
      });
    }, 100);
  }

  handleClick() {
    const { handleError } = this.props;
    // this.state = {
    //   origin: {
    //     lat: 1.28444,
    //     lng: 103.843376,
    //   },
    //   destination: {
    //     lat: 1.300693,
    //     lng: 103.856239,
    //   },
    // };
    const { origin, destination } = this.state;

    const isInSingapore = ({ lat, lng }) => {
      // throw error when the location is not in SG
      if (lat <= 1.2 || lat >= 1.4) {
        return false;
      }
      return _.floor(lng) === 103;
    };

    if (!origin || !destination) {
      handleError(true, 'Please input valid origin and destination!');
      return;
    }
    if (!isInSingapore(origin) || !isInSingapore(destination)) {
      handleError(true, 'Location not in Singapore!');
      return;
    }
    handleError(false); // clear error status
    const { handleClickFindRoutes } = this.props;
    handleClickFindRoutes(origin, destination);
  }

  handleClear() {
    const { handleError, clearResults } = this.props;

    this.setState({
      origin: null,
      destination: null,
    });
    handleError(false); // clear error status
    this.refs.origin.value = '';
    this.refs.destination.value = '';
    clearResults();
  }

  handleDestinationKeyPress(e) {
    // if user type Enter, same as 'handleClick'
    const { destination } = this.state;
    if (e.key === 'Enter' && destination) {
      this.handleClick();
    }
  }

  render() {
    return (
      <div id="input-box" className="box">
        <div className="field">
          <div className="control">
            <input
              id="origin"
              className="input"
              ref="origin"
              placeholder="Please choose your origin."
            />
          </div>
        </div>
        <div className="field">
          <div className="control">
            <input
              id="destination"
              ref="destination"
              className="input"
              placeholder="Please choose your destination."
              onKeyPress={this.handleDestinationKeyPress}
            />
          </div>
        </div>
        <div className="columns is-mobile">
          <div className="column">
            <button className="button is-link" type="submit" id="findBtn" onClick={this.handleClick}>Find routes</button>
          </div>
          <div className="column">
            <button className="button is-danger" type="button" id="clearBtn" onClick={this.handleClear}>Clear</button>
          </div>
        </div>
      </div>
    );
  }
}

InputBox.propTypes = {
  handleClickFindRoutes: PropTypes.func.isRequired,
  handleError: PropTypes.func.isRequired,
  clearResults: PropTypes.func.isRequired,
};

export default InputBox;
