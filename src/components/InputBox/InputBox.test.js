import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-15.4';
import TestRenderer from 'react-test-renderer';
import InputBox from '.';

Enzyme.configure({ adapter: new Adapter() });

describe('InputBox', () => {
  it('should match snapshot', () => {
    const mockPropsFunction = jest.fn();
    const mockHandleError = jest.fn();
    const clearResults = jest.fn();
    const tree = TestRenderer
      .create(
        <InputBox
          handleClickFindRoutes={mockPropsFunction}
          handleError={mockHandleError}
          clearResults={clearResults}
        />,
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('can trigger handleError function by click button', () => {
    const mockPropsFunction = jest.fn();
    const mockHandleError = jest.fn();
    const clearResults = jest.fn();
    const wrapper = shallow(<InputBox
      handleClickFindRoutes={mockPropsFunction}
      handleError={mockHandleError}
      clearResults={clearResults}
    />);

    wrapper.find('#findBtn').simulate('click');
    expect(mockHandleError).toBeCalled();
    expect(mockPropsFunction).not.toBeCalled();
  });

  it('can trigger handleError function when input location is not in SG', () => {
    const mockPropsFunction = jest.fn();
    const mockHandleError = jest.fn();
    const clearResults = jest.fn();
    const wrapper = shallow(<InputBox
      handleClickFindRoutes={mockPropsFunction}
      handleError={mockHandleError}
      clearResults={clearResults}
    />);

    wrapper.setState({
      origin: {
        lat: 0, lng: 0,
      },
      destination: {
        lat: 1, lng: 1,
      },
    });
    wrapper.find('#findBtn').simulate('click');
    expect(mockPropsFunction).not.toBeCalled();
    expect(mockHandleError).toBeCalled();
  });

  it('can trigger handleClick function when state is legal', () => {
    const mockPropsFunction = jest.fn();
    const mockHandleError = jest.fn();
    const clearResults = jest.fn();
    const wrapper = shallow(<InputBox
      handleClickFindRoutes={mockPropsFunction}
      handleError={mockHandleError}
      clearResults={clearResults}
    />);

    wrapper.setState({
      origin: {
        lat: 1.28444,
        lng: 103.843376,
      },
      destination: {
        lat: 1.300693,
        lng: 103.856239,
      },
    });
    wrapper.find('#findBtn').simulate('click');
    expect(mockPropsFunction).toBeCalled();
    expect(mockHandleError).toBeCalled();
  });
});
