import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-15.4';

import App from './App';

Enzyme.configure({ adapter: new Adapter() });

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
});

describe('Basic functionality', () => {
  it('can get search result when given origin and destination', () => {
    const wrapper = mount(<App />);
    expect(wrapper.find('#results-box').text()).toBe('');
    wrapper.instance().handleClickFindRoutes({
      lat: 1.281321,
      lng: 103.83897,
    }, {
      lat: 1.312078,
      lng: 103.796208,
    });
    const resultsText = wrapper.find('#results-box').text();
    expect(resultsText).not.toBe('');
    // expect(resultsText).toContain('Outram Park'); // contains start station
    // expect(resultsText).toContain('Holland Village'); // contains end station
  });
});
