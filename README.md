## SG MRT tool

### How to use
+ Prerequisites: have `node` and `yarn` installed in your local env.
+ Run `yarn && yarn start`, then check 'http://localhost:3000/' in your local browser.

You can check the attached screenshot gif for a quick browsing.
![demo](mark-zendesk-fe.gif) 

### Future todo list
* [x] Eslint
* [x] User experience using UI components
* [x] Unscrollable web app
* [x] Snapshot tests
* [x] Consider penalty for change routes
* [x] More error handling
* [x] Whether a location is in Singapore or not?
* [x] Support mobile screen
* [ ] Support less in webpack config (I use watcher to compile to css files as a workaround now.)
* [ ] User's history query
* [ ] Start from my position (needs Google Map API)

